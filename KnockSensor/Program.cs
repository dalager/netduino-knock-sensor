﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;

namespace KnockSensor
{
	public class Program
	{
		public static void Main()
		{
			try
			{
				var ks = new KnockSensor();
				ks.Run();
			}
			catch (Exception e)
			{
				Debug.Print(e.Message);
				Debug.Print(e.StackTrace);
			}
		}
	}

	public class KnockSensor
	{
		private readonly Queue Knoqueue = new Queue();


		private const double PiezoSensorThreshold = 0.09;


		/// <summary>
		/// Maximum knocks in knock sequence
		/// </summary>
		private const int MaxKnocks = 10;

		/// <summary>
		/// Minimum knock length in ms
		/// </summary>
		private const int MinKnockLength = 100;

		/// <summary>
		/// When to trigger either a post to the service or a cleaning of the queue
		/// </summary>
		readonly TimeSpan _knockTimeout = new TimeSpan(10000000); // 1 sec

		/// <summary>
		/// The endpoint that will receive a POST with an JSON serialized int array
		/// </summary>
		private const string BaseUrl = "http://knockservice.azurewebsites.net";
		private const string KnockServiceEndpoint = BaseUrl + "/api/knocks";
		private const string MaxInputServiceEndpoint = BaseUrl + "/api/maxinput";

		private  DateTime _lastKnock;

		private void SendMaxInput(double mi)
		{
			
			var json = "{\"value\":" + mi + "}";
			try
			{
				new Thread(() => PostToKnockService(json, MaxInputServiceEndpoint)).Start();
			}
			catch (Exception ex)
			{
				Debug.Print(ex.Message);
			}
		}

		private  void SendKnocks()
		{
			if (Knoqueue.Count <= 1)
				return;
			Debug.Print("Sending knocks");

			TimeSpan[] timespans = GetPauses();
			var sb = new StringBuilder();
			sb.Append("[");
			for (int index = 0; index < timespans.Length; index++)
			{
				TimeSpan timespan = timespans[index];
				var milliseconds = (int)(timespan.Ticks * 0.0001);
				if (milliseconds > MinKnockLength)
				{
					sb.Append(milliseconds);
					if (index != timespans.Length - 1)
					{
						sb.Append(",");
					}
				}
			}
			sb.Append("]");
			Debug.Print("Posting knocks to external service");
			Debug.Print(sb.ToString());
			try
			{
				new Thread(() => PostToKnockService(sb.ToString(), KnockServiceEndpoint)).Start();
			}
			catch (Exception ex)
			{
				Debug.Print(ex.Message);
			}

			Knoqueue.Clear();
		}

		


		public static void PostToKnockService(string json, string endpoint)
		{
			Debug.Print("Webreq thread start, sending to "+endpoint+": ");
			Debug.Print(json);
			try
			{
				byte[] buffer = Encoding.UTF8.GetBytes(json);
				var webRequest = (HttpWebRequest)WebRequest.Create(endpoint);
				webRequest.Accept = "application/json";
				webRequest.ContentType = "application/json";
				webRequest.Method = "POST";
				webRequest.KeepAlive = false;
				webRequest.ContentLength = buffer.Length;
				Stream datastream = webRequest.GetRequestStream();
				datastream.Write(buffer, 0, buffer.Length);
				datastream.Close();
				var resp = (HttpWebResponse)webRequest.GetResponse();
				var httpStatusCode = resp.StatusCode;
				Debug.Print("Answered " + httpStatusCode);
			}
			catch (Exception e)
			{
				Debug.Print(e.Message);
				Debug.Print(e.StackTrace);
			}
			Debug.Print("Webreq thread end");
		}


		public TimeSpan[] GetPauses()
		{
			var spans = new TimeSpan[Knoqueue.Count - 1];
			object[] datetimes = Knoqueue.ToArray();
			var dtArr = new DateTime[datetimes.Length];
			for (int i = 0; i < datetimes.Length; i++)
			{
				dtArr[i] = (DateTime)datetimes[i];
			}

			for (int i = 0; i < dtArr.Length; i++)
			{
				DateTime a = dtArr[i];

				if (i == dtArr.Length - 1)
				{
					return spans;
				}
				DateTime b = dtArr[i + 1];
				spans[i] = b - a;
			}
			return spans;
		}

		public void Run()
		{
			Debug.Print("Starting");
			var led = new OutputPort(Pins.ONBOARD_LED, false);
			var sensor = new AnalogInput(Cpu.AnalogChannel.ANALOG_0);
			double max = 0.0;
			
			bool ledOn = false;
			// with 22kohm, værdier ml 0.002 og 0.24
			while (true)
			{
				TimeSpan timeSinceLastKnock = (DateTime.Now -_lastKnock);
				if (timeSinceLastKnock > _knockTimeout)
				{
					if (Knoqueue.Count > 1)
					{
						SendKnocks();
					}
					else
					{
						Knoqueue.Clear();
					}
				}

				double piezoInput = sensor.Read();
				if (piezoInput > PiezoSensorThreshold)
				{
					ledOn = !ledOn;
					led.Write(ledOn);
					Debug.Print("Knock! (" + piezoInput + "), max="+max+", " + DateTime.Now);
					if (Knoqueue.Count ==MaxKnocks)
					{
						Knoqueue.Dequeue();
					}
					Knoqueue.Enqueue(DateTime.Now);
					_lastKnock = DateTime.Now;
					Thread.Sleep(MinKnockLength); // block double readings
				}

				if (piezoInput > max)
				{
					max = piezoInput;
					Debug.Print("New Max: " + piezoInput.ToString());
					SendMaxInput(max);
				}
			}
		}
	}
}
