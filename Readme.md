Netduino Knock Sensor
================

Testing an Netduino Plus as a knock sensor.

The sensor is a Piezo Element and the knocks are posted as an array of ms ints to an external service.

There are variables for

 * removing ultra short knocks (default <100 ms)
 * maximum knock sequence (default 10)
 * timeout before a knock sequence is deemed "finished" aka max pause between knocks (default 1000 ms)


Running Micro Framework 4.2 on 4.2 firmware.